#!/bin/bash
. ./cmd.sh
. ./path.sh
set -e
source path.sh

steps/make_fbank.sh --nj 1 data/ exp/fbank_log make_fbank/
steps/compute_cmvn_stats.sh data/ exp/fbank_log make_fbank/

apply-cmvn --utt2spk=ark:data/utt2spk scp:data/cmvn.scp scp:data/feats.scp ark,t:fbank.txt
